import 'package:injectable/injectable.dart';
import 'package:demo_app/common/models/use_case.dart';
import 'package:demo_app/domain/omdb/entities/omdb_entity.dart';
import 'package:demo_app/domain/omdb/repository/omdb_repository.dart';
import 'package:meta/meta.dart';

@lazySingleton
@injectable
class GetOmdbUsecase implements UseCase<List<OmdbEntity>, Map<String, String>> {
  final OmdbRepository omdbRepository;

  GetOmdbUsecase({@required this.omdbRepository});

  @override
  Future<List<OmdbEntity>> call(Map payload) {
    return omdbRepository.searchOmdbByTitleYear(payload['apikey'] ?? 'unknownkey', title: payload['title'], year: payload['year']);
  }
}
