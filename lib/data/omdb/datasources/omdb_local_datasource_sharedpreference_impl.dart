import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:demo_app/data/omdb/models/omdb_model.dart';

import 'omdb_local_datasource.dart';

class OmdbLocalDatasourceSharedPreferenceImpl
    implements OmdbLocalDatasource {
  final SharedPreferences sharedPreferences;

  OmdbLocalDatasourceSharedPreferenceImpl(
      {@required this.sharedPreferences});

  @override
  Future<List<Omdb>> getOmdbsByTitleYear(String titleyear) async {
    final jsonString = sharedPreferences.getString('omdb_cache_$titleyear');
    if (jsonString != null) {
      List<dynamic> rawList = jsonDecode(jsonString);
      List<Omdb> asteroidList = rawList
          .map<Omdb>((dynamic rawItem) =>
              Omdb.fromJson(rawItem as Map<String, dynamic>))
          .toList();
          
      return asteroidList;
    }
    return null;
  }

  @override
  Future<void> setOmdbsByTitleYear(String titleyear, List<Omdb> omdbs) async {
    if (omdbs == null) {
      await sharedPreferences.remove('omdb_cache_$titleyear');
    } else {
      final rawList = omdbs
          .map<Map<String, dynamic>>((Omdb asteroid) => asteroid.toJson())
          .toList();
          //.cast<Map<String, dynamic>>();
      final json = jsonEncode(rawList);
      await sharedPreferences.setString('omdb_cache_$titleyear', json);
    }
  }
}
