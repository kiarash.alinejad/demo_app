
import 'package:demo_app/data/omdb/models/omdb_model.dart';
import 'dart:convert';
import 'package:injectable/injectable.dart';
import 'package:demo_app/common/network/omdb_ws_client.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;

@Bind.toType(OmdbRemoteDatasourceImpl)
@injectable
abstract class OmdbRemoteDatasource {
  Future<List<Omdb>> searchOmdbByTitleYear(String apikey, {String title, String year});
}

@lazySingleton
@injectable
class OmdbRemoteDatasourceImpl implements OmdbRemoteDatasource {
  final OmdbWsClient client;

  OmdbRemoteDatasourceImpl({@required this.client});

  factory OmdbRemoteDatasourceImpl.create() {
    return OmdbRemoteDatasourceImpl(
      client: OmdbWsClientImpl(http.Client()),
    );
  }

  @override
  Future<List<Omdb>> searchOmdbByTitleYear(String apikey, {String title, String year}) async {
    
    Uri uri = Uri.https('www.omdbapi.com', '', {
      'apikey': apikey,
      's': title,
      'y': year,
    });
    final response = await client.get(uri);
    String json = response.body;

    List<Omdb> omdbs = [];

    Map<String, dynamic> decodedJson = jsonDecode(json);
    List<dynamic> searchDecoded = decodedJson['Search'];
    if(searchDecoded!=null){
      searchDecoded.forEach((listItem) {
        // listItem.forEach((key, value) {
          omdbs.add(Omdb.fromJson(listItem as Map<String, dynamic>));
        // });
      });
    }
    return omdbs;
  }
}

