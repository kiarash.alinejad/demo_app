part of 'video_bloc.dart';

@immutable
abstract class FavoriteVideoEvent {}

@immutable
class FavoriteVideoLoadTodayEvent extends FavoriteVideoEvent {
}

@immutable
class FavoriteVideoAddEvent extends FavoriteVideoEvent {
  final String token;
  final OmdbEntity omdb;

  FavoriteVideoAddEvent(this.token,this.omdb);
}

