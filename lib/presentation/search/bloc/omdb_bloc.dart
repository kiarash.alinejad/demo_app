import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:demo_app/domain/omdb/entities/omdb_entity.dart';
import 'package:demo_app/domain/omdb/usecases/omdb_usecases.dart';

part 'omdb_event.dart';
part 'omdb_state.dart';

@injectable
class OmdbBloc extends Bloc<OmdbEvent, OmdbState> {
  final GetOmdbUsecase getOmdbUsecase;

  OmdbBloc({@required this.getOmdbUsecase});

  @override
  OmdbState get initialState => OmdbInitialState();

  @override
  Stream<OmdbState> mapEventToState(
    OmdbEvent event,
  ) async* {
    if(event is OmdbLoadByTitleYearEvent){
      yield* _loadOmdbByTitleYear(event.apikey, title:event.title, year:event.year);
    }
    else{
      yield OmdbDataState(omdbList: List<OmdbEntity>());
    }
  }

  Stream<OmdbState> _loadOmdbByTitleYear(String apikey, {String title, String year}) async* {
    yield OmdbLoadingState(apikey, title: title, year: year);
    try {
      Map payload = {'apikey':apikey, 'title':title, 'year':year };
      final omdbList = await getOmdbUsecase(payload);
      yield OmdbDataState(omdbList: omdbList);
    } catch (e) {
      yield OmdbErrorState(e);
    }
  }
}
