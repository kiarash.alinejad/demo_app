import 'dart:convert';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:demo_app/data/favorite/models/video_model.dart';
import 'package:demo_app/domain/favorite/entities/video_entity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_keychain/flutter_keychain.dart';
import 'package:demo_app/common/utils/keychain.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'bloc/dashboard_bloc.dart';

class DashboardPage extends StatefulWidget {
  const DashboardPage({Key key}) : super(key: key);
  @override
  _DashboardPageState createState() => _DashboardPageState();
}

class _DashboardPageState extends State<DashboardPage> {
  bool _showRecommended;
  String _token;

  initKeychainApikey() async{
    var token = await FlutterKeychain.get(key: MyKeychain.TOKEN);
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    if (mounted) {
      setState(() {
        _showRecommended = prefs.getBool(MyKeychain.RECOMMENDED) ?? true;
        _token = token;
        BlocProvider.of<DashboardBloc>(context).add(DashboardRefreshEvent(_token));
      });
    }
  }

  @override
  void initState() {
    super.initState();
    initKeychainApikey();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _showNetworkErrorDialog() {
    showDialog(
        context: context,
        child: new AlertDialog(
          title: new Text("No Connection"),
          content: new Text("Network connection error, try again later"),
          actions: <Widget>[
            FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  BlocProvider.of<DashboardBloc>(context).add(DashboardRefreshEvent(_token));
                },
                child: Text('Retry')),
            FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('Cancel')),
          ],
        ));
  }

  Widget _buildProgressBar() {
    return Center(child: CircularProgressIndicator());
  }

  Widget _FavoritePoster(String uri){
    if(uri==null || uri=='N/A'){
      return Icon(Icons.image);
    }
    else{
      // CachedNetworkImage(
      //   imageUrl: '$uri',
      //   imageBuilder: (context, imageProvider) => Container(
      //     decoration: BoxDecoration(
      //       image: DecorationImage(
      //           image: imageProvider,
      //           fit: BoxFit.cover,
      //           colorFilter:
      //               ColorFilter.mode(Colors.red, BlendMode.colorBurn)),
      //     ),
      //   ),
      //   placeholder: (context, url) => CircularProgressIndicator(),
      //   errorWidget: (context, url, error) => Icon(Icons.error),
      // );
      return CachedNetworkImage(
        imageUrl: '$uri',
        placeholder: (context, url) => CircularProgressIndicator(),
        errorWidget: (context, url, error) => Icon(Icons.error),
        fit: BoxFit.cover,
      );
    }
  }

  Widget _RecommendedMovies(List<VideoEntity> recommendedList){
    return Swiper(
        itemBuilder: (BuildContext context,int index){
          VideoEntity currentVideo = recommendedList[index];
          // return new Image.network(currentVideo.poster,fit: BoxFit.cover,);
          return _FavoritePoster(currentVideo.poster);
        },
        itemCount: recommendedList.length,
        pagination: new SwiperPagination(),
        control: new SwiperControl(),
        viewportFraction: 0.8,
        scale: 0.9,
      );
  }

  @override
  Widget build(BuildContext context) {

    return SafeArea(
      child: 
      BlocConsumer<DashboardBloc, DashboardState>(
        listener: (context, state) {
          if (state is DashboardErrorState) {
            _showNetworkErrorDialog();
          } else if (state is DashboardLoadingState) {
            return _buildProgressBar();
          }
        },
        builder: (context, state) {

          if (state is DashboardDataState) {
            if(_showRecommended){
              return Scaffold(
                body:  _RecommendedMovies(state.recommendedList),
              );
            }
            else{
              return Scaffold(
                body:  Text(''),
              );
            }
          } else {
            return _buildProgressBar();
          }
        },
      ),
    );
  }
}