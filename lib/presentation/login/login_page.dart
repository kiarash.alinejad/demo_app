import 'package:flutter/material.dart';
import 'package:demo_app/common/routes/routes.dart';
import 'package:demo_app/common/utils/keychain.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:flutter_keychain/flutter_keychain.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String _apikey;
  String _token;

  final tokenController = TextEditingController();
  final apikeyController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    apikeyController.dispose();
    tokenController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    if (isKeyTokenExist() == true) {
      Navigator.of(context).pushNamed(Routes.homePage, arguments:0);
    }
  }

  isKeyTokenExist() async {
    var apikey = await FlutterKeychain.get(key: MyKeychain.APIKEY);
    var token = await FlutterKeychain.get(key: MyKeychain.TOKEN);

    //key and token exist, reuse them
    if (apikey != null && token != null) {
      if (mounted) {
        setState(() {
          _apikey = apikey;
          _token = token;
        });
      }
      return true;
    } else {
      //get from textfield
      return false;
    }
  }

  overwriteKeyToken(String apikey, String token) async {
    if (mounted) {
      setState(() {
        _apikey = apikey;
        _token = token;
      });
      await FlutterKeychain.put(key: MyKeychain.APIKEY, value: _apikey);
      await FlutterKeychain.put(key: MyKeychain.TOKEN, value: _token);
    }
  }

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'logo',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 48.0,
        child: SvgPicture.asset('images/logo.svg'),
      ),
    );

    var email = TextFormField(
      validator: (value) {
        if (value.isEmpty) {
          return 'Please enter some text';
        }
        if (!value.contains('@') && !value.contains('.')) {
          return 'Please enter valid token';
        }
        return null;
      },
      controller: tokenController,
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'youremail@dkatalis.com',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    var password = TextFormField(
      validator: (value) {
        if (value.isEmpty) {
          return 'Please enter some text';
        }
        return null;
      },
      controller: apikeyController,
      autofocus: false,
      // initialValue: 'some password',
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'apikey',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 8.0),
      child: Material(
        // borderRadius: BorderRadius.circular(25.0),
        shadowColor: Colors.redAccent.shade400,
        // elevation: 3.0,
        child: MaterialButton(
          minWidth: 200.0,
          height: 42.0,
          onPressed: () {
            if (_formKey.currentState.validate()) {
              //store token and apikey
              overwriteKeyToken(apikeyController.text, tokenController.text);
              Navigator.of(context).pushNamed(Routes.homePage, arguments: 0);
            }
          },
          color: Colors.redAccent.shade700,
          child: Text('Log In', style: TextStyle(color: Colors.white)),
        ),
      ),
    );

    return Scaffold(
        backgroundColor: Colors.white,
        body: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
              Center(
                child: ListView(
                  shrinkWrap: true,
                  padding: EdgeInsets.only(left: 24.0, right: 24.0),
                  children: <Widget>[
                    logo,
                    SizedBox(height: 48.0),
                    email,
                    SizedBox(height: 8.0),
                    password,
                    SizedBox(height: 24.0),
                    loginButton,
                  ],
                ),
              ),
            ])));
  }
}
