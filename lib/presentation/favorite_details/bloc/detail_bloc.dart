import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:demo_app/common/models/use_case.dart';
import 'package:demo_app/domain/favorite/entities/video_entity.dart';
import 'package:demo_app/domain/favorite/usecases/video_usecases.dart';

part 'detail_event.dart';
part 'detail_state.dart';

@injectable
class DetailBloc
    extends Bloc<DetailEvent, DetailState> {
  final GetVideoByIdUsecase getVideoByIdUsecase;
  final UpdateVideoUsecase updateVideoUsecase;

  DetailBloc({
    this.getVideoByIdUsecase,
    this.updateVideoUsecase,
  });

  @override
  DetailState get initialState =>
      DetailInitialState();

  @override
  Stream<DetailState> mapEventToState(
    DetailEvent event,
  ) async* {
    if (event is DetailRefreshEvent) {
      yield DetailLoadingState();
      try {
        final String token = event.token;
        final VideoEntity video = event.currentVideo;
        final Map<String, dynamic> payload = {'token':token, 'id': video.id };
        final liveVideo = await getVideoByIdUsecase(payload);
        yield DetailDataState(currentFavoriteVideo: liveVideo);
      } catch (e) {
        yield DetailErrorState(e);
      }
    } else if (event is DetailUpdateEvent) {
      yield DetailLoadingState();
      try {
        String token = event.token;
        VideoEntity video = event.favoriteVideoEntity;
        Map<String, dynamic> payload = {'token':token, 'video':video};
        await updateVideoUsecase(payload);
        // final liveVideo = await getVideoByIdUsecase(payload); //search again from api to ensure what has been saved indeed saved online
        yield DetailDataState(currentFavoriteVideo: video); 
      } catch (e) {
        yield DetailErrorState(e);
      }
    }
  }
}
