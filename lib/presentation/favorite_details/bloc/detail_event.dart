part of 'detail_bloc.dart';

@immutable
abstract class DetailEvent {}

@immutable
class DetailRefreshEvent extends DetailEvent{
  final String token;
  final VideoEntity currentVideo;
  
  DetailRefreshEvent(this.token, this.currentVideo);
}

@immutable
class DetailUpdateEvent extends DetailEvent{
  final String token;
  final VideoEntity favoriteVideoEntity;

  DetailUpdateEvent(this.token, this.favoriteVideoEntity);
}

