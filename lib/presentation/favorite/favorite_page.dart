import 'package:cached_network_image/cached_network_image.dart';
import 'package:demo_app/common/routes/routes.dart';
import 'package:demo_app/domain/favorite/entities/video_entity.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_keychain/flutter_keychain.dart';
import 'package:demo_app/common/utils/keychain.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

import 'bloc/video_bloc.dart';

class FavoritePage extends StatefulWidget {
  const FavoritePage({Key key}) : super(key: key);
  @override
  _FavoritePageState createState() => _FavoritePageState();
}

class _FavoritePageState extends State<FavoritePage> {

  String _token;
  // List<VideoEntity> _videoList;
  // final titleController = TextEditingController();
  // final yearController = TextEditingController();


  initKeychainApikey() async{
    var token = await FlutterKeychain.get(key: MyKeychain.TOKEN);
    if (mounted) {
      setState(() {
        _token = token;
        BlocProvider.of<FavoriteVideoListBloc>(context).add(VideoListRefreshEvent(_token));
      });
    }
  }

  @override
  void initState() {
    super.initState();
    initKeychainApikey();
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    // titleController.dispose();
    // yearController.dispose();
    super.dispose();
  }

  void _showNetworkErrorDialog() {
    showDialog(
        context: context,
        child: new AlertDialog(
          title: new Text("No Connection"),
          content: new Text("Network connection error, try again later"),
          actions: <Widget>[
            FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  BlocProvider.of<FavoriteVideoListBloc>(context)
                      .add(VideoListRefreshEvent(_token));
                },
                child: Text('Retry')),
            FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: Text('Cancel')),
          ],
        ));
  }

  Widget _buildProgressBar() {
    return Center(child: CircularProgressIndicator());
  }

  Widget _FavoritePoster(String uri){
    if(uri==null || uri=='N/A'){
      return Icon(Icons.image);
    }
    else{
      return CachedNetworkImage(
        imageUrl: '$uri',
        placeholder: (context, url) => CircularProgressIndicator(),
        errorWidget: (context, url, error) => Icon(Icons.error),
        fit: BoxFit.cover,
      );
    }
  }

  Widget _buildListView(List<VideoEntity> videos) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
      child: ListView.builder(
        itemBuilder: (context, index) {
          VideoEntity video = videos[index];
          
          return Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Dismissible(
              key: Key(video.id), 
              background: Container(color: Colors.red),
              onDismissed: (direction) {
                setState(() {
                  BlocProvider.of<FavoriteVideoListBloc>(context).add(VideoListDeleteEvent(_token, video));
                });

                Scaffold
                    .of(context)
                    .showSnackBar(SnackBar(content: Text("${video.title} removed from Favorites!")));
              },
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Flexible(child: 
                            Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  video.title,
                                  style: Theme.of(context).textTheme.headline,
                                ),
                                Text(
                                  'Year: ${video.year}',
                                  style: Theme.of(context).textTheme.body2,
                                ),
                                Text(
                                  'Label: ${video.label}',
                                  style: Theme.of(context).textTheme.body1,
                                ),
                                // SizedBox(height: 2.0),
                                SmoothStarRating(
                                  allowHalfRating: false,
                                  starCount: 5,
                                  rating: video.rating.toDouble(),
                                  size: 16.0,
                                  filledIconData: Icons.star,
                                  halfFilledIconData: Icons.star_half,
                                  defaultIconData: Icons.star_border,
                                  color: Colors.redAccent,
                                  borderColor: Colors.redAccent,
                                  spacing:0.0
                                ),
                                Text('Priority ${video.priority}')
                                // SizedBox(height: 5.0),
                                // SmoothStarRating(
                                //   allowHalfRating: false,
                                //   starCount: 5,
                                //   rating: video.priority.toDouble(),
                                //   size: 16.0,
                                //   filledIconData: Icons.add_circle,
                                //   halfFilledIconData: Icons.add_circle_outline,
                                //   defaultIconData: Icons.add_circle,
                                //   color: Colors.redAccent,
                                //   borderColor: Colors.redAccent,
                                //   spacing:0.0
                                // ),
                              ],
                            ),
                          ),
                          SizedBox(width: 10.0),
                          Flexible(
                            child: _FavoritePoster(video.poster),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          IconButton(
                            icon: Icon(Icons.edit),
                            tooltip: 'Edit Favorite Movie',
                            onPressed: () async {
                              final updateSuccessful = await Navigator.of(context).pushNamed(Routes.favoriteDetailPage, arguments: video);
                              if (updateSuccessful != null && updateSuccessful){
                                BlocProvider.of<FavoriteVideoListBloc>(context).add(VideoListRefreshEvent(_token));
                              }
                            },
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            )
          );
        },
        itemCount: videos != null ? videos.length : 0,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: 
      BlocConsumer<FavoriteVideoListBloc, FavoriteVideoListState>(
        listener: (context, state) {
          if (state is FavoriteVideoListErrorState) {
            _showNetworkErrorDialog();
          } else if (state is FavoriteVideoListLoadingState) {
            return _buildProgressBar();
          }
        },
        builder: (context, state) {
          if (state is FavoriteVideoListDataState) {
            // _videoList = state.favoriteVideoList;
            return Column(
                    children: <Widget>[
                      // searchBar,
                      Expanded(child: _buildListView(state.favoriteVideoList),),
                    ],
                  );
          } 
          else if (state is FavoriteVideoListDeleteDataState) {
            // _videoList = state.favoriteVideoList;
            return Column(
                    children: <Widget>[
                      // searchBar,
                      Expanded(child: _buildListView(state.favoriteVideoList),),
                    ],
                  );
          }
          else {
            return _buildProgressBar();
          }
        },
      ),
    );

    // var tfTitle = TextFormField(
    //   controller: titleController,
    //   keyboardType: TextInputType.text,
    //   autofocus: false,
    //   decoration: InputDecoration(
    //     hintText: 'Movie Title',
    //     contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
    //     border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
    //   ),
    // );

    // var tfYear = TextFormField(
    //   controller: yearController,
    //   keyboardType: TextInputType.text,
    //   autofocus: false,
    //   decoration: InputDecoration(
    //     hintText: 'Movie Year',
    //     contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
    //     border: OutlineInputBorder(borderRadius: BorderRadius.circular(32.0)),
    //   ),
    // );

    // var searchBar =  
    //   Container(
    //     padding: const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
    //     child: Row(
    //       children: <Widget>[
    //         Expanded(
    //           child: tfTitle,
    //         ),
    //         Expanded(
    //           child: tfYear,
    //         ),
    //         IconButton(
    //           icon: Icon(Icons.search),
    //           tooltip: 'Search Movie',
    //           onPressed: () {
    //             searchState(titleController.text, yearController.text);
    //           },
    //         ),
    //       ],
    //     )
    //   );
    
  }
}