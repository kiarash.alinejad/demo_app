import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';
import 'package:demo_app/common/models/use_case.dart';
import 'package:demo_app/domain/favorite/entities/video_entity.dart';
import 'package:demo_app/domain/favorite/usecases/video_usecases.dart';

part 'video_event.dart';
part 'video_state.dart';

@injectable
class FavoriteVideoListBloc
    extends Bloc<VideoListEvent, FavoriteVideoListState> {
  final GetAllVideosUsecase getAllFavoriteVideosUsecase;
  final DeleteVideoUsecase deleteFavoriteVideoUsecase;

  FavoriteVideoListBloc({
    this.getAllFavoriteVideosUsecase,
    this.deleteFavoriteVideoUsecase,
  });

  @override
  FavoriteVideoListState get initialState =>
      FavoriteVideoListInitialState();

  @override
  Stream<FavoriteVideoListState> mapEventToState(
    VideoListEvent event,
  ) async* {
    if (event is VideoListRefreshEvent) {
      yield FavoriteVideoListLoadingState();
      try {
        String token = event.token;
        final favoriteList = await getAllFavoriteVideosUsecase(token);
        yield FavoriteVideoListDataState(favoriteVideoList: favoriteList);
      } catch (e) {
        yield FavoriteVideoListErrorState(e);
      }
    } else if (event is VideoListDeleteEvent) {
      yield FavoriteVideoListLoadingState();
      try {
        String token = event.token;
        VideoEntity video = event.favoriteVideoEntity;
        Map<String, dynamic> map = {'token':token, 'video':video};
        await deleteFavoriteVideoUsecase(map);
        final favoriteList = await getAllFavoriteVideosUsecase(token);
        yield FavoriteVideoListDeleteDataState(favoriteVideoList: favoriteList);
      } catch (e) {
        yield FavoriteVideoListErrorState(e);
      }
    }
  }
}
