# FLUTTER WORKSHOP

## DAY 1

Install Android Studio	https://developer.android.com/studio/install
Install Xcode	https://developer.apple.com/xcode/resources/
Install Flutter SDK	https://flutter.dev/docs/get-started/install
Install Vscode with Dart and Flutter extensions	https://code.visualstudio.com/docs/setup/setup-overview
Review: Dart language tour	https://dart.dev/guides/language/language-tour
Write your First Flutter app	https://flutter.dev/docs/get-started/codelab
Review: Widgets of the week	https://www.youtube.com/watch?v=b_sQ9bMltGU&list=PLjxrf2q8roU23XGwz3Km7sQZFTdB996iG&index=1

https://github.com/flutter/flutter/wiki/The-Engine-architecture

https://github.com/AlexVegner/workshop

https://dartpad.dev

https://github.com/flutter/flutter/wiki/Flutter-build-release-channels

https://github.com/flutter/flutter/wiki/Multi-device-debugging-in-VS-Code

https://developer.android.com/studio/command-line/adb?hl=id

https://stackoverflow.com/questions/49047411/flutter-how-to-create-a-new-project

https://pub.dev/

http://omdbapi.com/

## DAY 2

Dart Language

https://dart.dev/tools

Some Backend framework based on flutter
https://aqueduct.io/docs/best_practices/
https://angel-dart.dev/
https://github.com/angel-example/flutter

https://dart.dev/guides/language/language-tour


Today we will go through Dart presentation at:
https://drive.google.com/file/d/1U-p0NG0vTYjE-_hot6sWugLDL70dA9sK/view?usp=sharing

```
///
/// Documentation comments style Google preferred
///

  List<double> list4 = list1.map((int e) => e + 1.2).toList().cast<double>();
  print(list4);


const p1 = Point(1,1);
  const p2 = Point(1,1);
class Point{
  final x;
  final y;
  const Point(this.x, this.y);
}
```

No Overload in Dart.
But you can Override in Dart.
The name of the function is the key.

https://gist.github.com/AlexVegner

```
/// Recomendation for models
/// Use final fields and named parameters in constructor
/// Protect your models from mutation
/// you could use @immutable, for meta package
class UserEntity {
  final String name;
  final int age;
  final String email;

  UserEntity({this.name, this.age, this.email});

  @override
  String toString() {
    // reuse toString
    return '$name, age: $age, $email';
  }
}
```

In state management like Redux, you should create immutable. 
CopyWith. 

Use named parameters.

## DAY 3 

Based on presentation

https://drive.google.com/open?id=14C-_xEr_zuJ0m-__kAcO-7FPZ6b7iAyf

Tips:
Compose your layout by sections / functions
Logically compose your layout with Container

1 file for every reusable widgets
1 file for 1 screen, all related screen widgets should be in that file

Dyma said he builds everything in MaterialApp. No need to care for CupertinoApp. Everything will auto adjust depending on the mobileOS where it runs.

Below will be your bible for the next months:
https://material.io/

Scaffold is mostly used for every screen/page. 

If no alignment is mentioned, default is 0,0 coordinate.

padding vs margin
padding = distance between current widget to other widgets
EdgeInsets.only(top left right bottom)
EdgeInsets.symmetric horizontal or vertical

Dyma said users like simple app. Custom gestures are not recommended. Beyond simple tap is harder for users. Any client without any additional tutorial/doc should be able to use it.

Stateful widget basically create 2 classes. 1 function createState() but will be a State class. State class has a build method. And only triggered by setState(). In Stateless widget we dont touch the build method. In Stateful we control it.
setState() => build()

BuildContext will be referred in the Stateful widget. And each time Stateful widget is calling setState, it creates the element in the build() method for the Element and the element needs to refer to BuildContext for properties information.

initState() is called only once.

so your stateful widget should implement the state modification in the didUpdateWidget(). 

Put your streams subscribe code in initState. And use dispose() method to cancel all those subscriptions. 

Use key for widgets if you want to reorder their location or position. Because the flutter will treat different objects from the same class as the same object without key identifier.

Must watch Key class video:
https://www.youtube.com/watch?v=kn0EOS-ZiIc

Typically the flow of State is from upper tree to lower tree. But with global Key, you can go against this pattern from the lower tree looking from the higher tree. The global keys are seldom used unless you need to reparent.

Tips: Try to keep your Widget tree the same. Some pointers:
Use IgnorePointer class and wrap its children. Then play with the boolean ignoring to change rather than changing your Widget tree.

LAYOUT
https://drive.google.com/open?id=1LY-BdZFr6VYGpmBRLSgkK27ruU5j04iB

Expanded is a case of Flexible. 

Dyma said its not possible to have 1 design that can cover also the bigger tablet/ipad screen. So the best practice is to check the size of the screen and then use different UI tree.

You manage look and feel with ThemeData in Material. 
example: MaterialApp property:
theme: ThemeData.dark().copyWith(primaryColor: Colors.red, accentColor: Colors.amberAccent),

debugPaintSizeEnabled = true 
this option is set at the main() and is very helpful to see the boundaries of all widgets.

Tips:
Render on demand, lazy load.
Use dynamiclistview for displaying many rows of data. It will only keep in memory the visible data.

_maxCrossAxisExtentGridView is usually used most of the time.

Generally Flutter download fonts online, so you want to download custom fonts and set them in the Assets folder. 

Tips:
For the omdbapi project, use Card class.

## DAY 4

Navigator

push = Taking a page and put it on top of the current page
pop = Remove current page and Going back to previous page 
pushReplacement = Taking a page and put it on top of the current page and then remove the current page
pushNamed = You can use name of the class of the widget

CupertinoPageRoute = putting a page on top of current but you can actually see the layering. A slightly different approach sliding from the bottom to the top. 
Modal pages. Has its own stack.

Add the AppBar you can disable the back button
leading: IconButton(icon: Icon(Icons.battery_charging_full), onPressed(){
    Navigator.of(context).push(
       MaterialPageRoute(
          fullscreendialog: true,
          builder: (context) => SecondPage()
       )
    );
})

===================================
Dyma presents the best practice Routing for Flutter. Creating a Routing system below:

initialRoute = '/' This is usually home page
onGenerateRoute: RouteGenerator.generateRoute, 

create class
class RouteGenerator{
  static Route<dynamic> generateRoute(RouteSettings settings){
    final args = settings.arguments;
    switch (settings.name){
       case Routes.loginPage:
          if(args is String){
             builder: (_) => LoginPage(title: args);
          }

    }
  }
}

abstract class Routes{
  static constant String loginPage = '/login';
  static constant String loginPageDetail = '$loginPage/detail'
}
Tips why? So this routing can also work for web, not just mobile.

Then you can do like this at the Home Page:

Navigator.of(context).pushNamed(Routes.loginPage, arguments: 'Second page through named route.

arguments should be Object type. So you can pass anything, and if multiple arguments occur you can use List or Map and pass it on. 

=================================

Dyma said 
Map<String, WidgetBuilder> is not very good approach for managing Routes because we cannot pass arguments easily to the routing page/widget.

======================
You should structure in this way in your lib folder:

lib > 
   pages > 
      login > 
         login.dart
         login_box.dart
         login_details.dart
      favorite > 
         favorite.dart
         favorite_animation.dart
         favorite_details.dart
   routes > 
     router.dart
     routes.dart

============================

drawer
Use MediaQuery to calculate the width of the drawer.

SafeArea
Keep off area so the UI will not draw items automatically in that area.

app://domain/home

================

Dyma said Best Flutter state management so far is: 
https://bloclibrary.dev/#/
And currently will be adopted by Google Flutter as the recommended state management.

In GridView
childAspectRatio is used to define the card, otherwise it will be square always.

Image
Has url property that can be linked to http and get images.
BoxFit = use this to tell fit property of Image. 
Example: 
BoxFit.none = original image size
BoxFit.
BoxFit.cover = It will use the smallest width/height and maintain ratio of the image
Cover is the most used method.

There is a package called CachedNetworkImage widget, which will cache the images. 

Container > BoxDecoration > ClipRRect/ClipOval/ClipCircle 
are typical combo widget tree used to customize look and feel of any Widget. Pay attention to borderRadius BorderRadius, BoxShadow, and Offset.

Tips: Clip is expensive operations, may be harmful to performance.

=======================

Avoid God widget. Widget that can does everything simply by playing with its properties. There should be a measured balance to have some flexibility by adding properties and customize.

=====================
NETWORK
https://drive.google.com/file/d/1cz3q6kCapd7YJLTYxekGH-3C8qE1FziW/view?usp=sharing

Tips:
Create your own http BaseClient.
Use Bloc for streaming event based 

class HomePage will have a file name home_page.dart

=====================

Secure your string 
https://pub.dev/packages/flutter_string_encryption

Secure your keys
https://pub.dev/packages/flutter_keychain

Version
https://pub.dev/packages/package_info

## DAY 5

Clean Architecture slides:
https://drive.google.com/open?id=1Gvk2LEP8XP0F4whUn5sJKe3pqWmbbE0e

Entities = Your UI model/objects
Models = The datasource model/objects
Repository will convert and map from Models to Entities.

Bloc is at the presentation layer.
API/gRPC/GraphQL will be at Provider at the data layer.
Repository is in between presentation and data layer which is called Domain.

3 folders:
Presentation 
Domain
Data
You will have features under each folder . Domain Driven Development. 

What we will use for Bloc:
https://pub.dev/packages/flutter_bloc

async* is used for Stream and Generator.

yield* is used when you want to call function.
For example in the counter:
yield state -1;

yield* decrement(state);

int decrement(state) async*{
   yield state -1;
}

We need BlocProvider. A class which contains all Bloc available in the application.

2 ways of using BlocProvider

1. Below method will not release YourBloc() upon releasing the YourPage() from memory.
BlocProvider.value(
  value: YourBloc(),
  child: YourPage()
)

2. Below method will remove YourBloc upon successful page instantiate. When we go out of YourPage and YourPage is released from memory, this YourBloc will be released from memory.
BlocProvider(
  create: (_) => YourBloc(),
  child: YourPage()
)


If you dont use BlocProvider, you can do it this way also:
BlocBuilder<XBloc, Ydata>(
bloc: XBloc()
)

MultiBlocProvider(
providers: [
   BlocProvider(),
  BlocProvider()
]
child: YourPage()
)

If you dont want to redraw the widget, but just get data.
BlocListener<YourBloc, Event>(listener: (context, state))
BlocListener also has condition property where you can write some condition based on the previous state and current state.

BlocConsumer(builder: , listener: )

final themeBloc = BlocProvider.of<ThemeBloc>(context); 
the same with
context.bloc<ThemeBloc> ...
Please refer to flutter_bloc package documentation.

DO NOT USE REPOSITORY PROVIDER in flutter_bloc.
It will break clean architecture because provider should not be in the UI.

DEPENDENCY INJECTION
Is used to isolate each Object purely so the unit test can be simplified. The code must be very clear separation between each other. No instance of other Object can be instantiated within. 

Slides:
https://drive.google.com/open?id=1F2iQapJNh3RaziDJ-Q8RsHavY4pnz202

Some depin packages:
https://pub.dev/packages/get_it
Get it. Oleks prefer this package.
https://pub.dev/packages/injectable
Use injectable for getit.

https://pub.dev/packages/kiwi
Last update of Kiwi last year ago. So Oleks recommends Getit instead. 

Olex prefers RegisterLazySingleton in Getit. 
Factory constructor. RegisterFactory. Multiple screen uses multiple Blocs. Each screen should have its own Bloc.
Data Layer and Domain Layer we will use mostly LazySingleton. Just 1 instance. 

Be careful with GetIt auto generator on BlocProvider. You need to ensure some Bloc to be using .value method so it will retain the Bloc upon exiting the page.

Moor generator currently does not support latest version of analyzer.

To generate code with flutter, we use build_runner:
flutter packages pub run build_runner build

To delete previous generated code:
flutter packages pub run build_runner clean

Please git pull again from workshop repository to get example of asteroid with Clean Architecture.

When you are an expert of Clean Architecture, the next level will be:
https://pub.dev/packages/ex_bloc

But for DK, please stick with Clean Architecture.

## DAY 6

You should limit the scope of your testing per object. If dependency, then you just create a test file for dependency, nothing more. If you test a bloc, then just test the bloc and fake everything relevant to it. Same thing with use case, datasource, etc. In this way your scope will not be too complex but at the same time covers all possible test combination.

Unit Test
https://pub.dev/packages/test

Bloc Test
https://pub.dev/packages/bloc_test
https://resocoder.com/2019/11/29/bloc-test-tutorial-easier-way-to-test-blocs-in-dart-flutter/
The purpose of this test is to expect the State should matches the Event. So when an Event happens, the State that comes out of the Bloc should match. Should still use Mock to simulate the related/relevant UseCase of the Bloc. 
The State should implements EquatableMixin/Comparable so the test function could compare the States. 

Mockito
https://pub.dev/packages/mockito

Create test folder in the root.
Then have test in folders:
data
domain
usecases
presentation

To run the test:
flutter test

It will auto look for test folder and execute all codes inside. You can also test individually by clicking the Run button on top of the each Test code function.

COVERAGE
flutter test --coverage
It will generate a report file: coverage/Icov.info 

http://blog.wafrat.com/test-coverage-in-dart-and-flutter/

WIDGET TEST
https://drive.google.com/open?id=14C-_xEr_zuJ0m-__kAcO-7FPZ6b7iAyf

SemanticLabel is used to enable pronunciation of the Text on the UI. The Android will be able to say the Widget UI values audibly. And it can also be used for testing.

The goal is to test if our UI will display correctly or as expected in all kinds of screen. For example a button could be covered by another component and cant be seen or touched.

Gherkin expression:
Given
When
Then

Care must be given in making the Widgets in the page structured in a way to enable Widget Test easily.

Tips to use Key for getting the widget to be tested. 

https://flutter.dev/docs/testing#integration-tests
Use Flutter Driver package
flutter_driver

BDD in Flutter
https://pub.dev/packages/flutter_gherkin

## DAY 7

RXDART
The base of flutter_bloc. Has some advanced features that may be useful in certain cases. 
https://www.burkharts.net/apps/blog/rxdart-magical-transformations-of-streams/

Debounce
Search fields. Debounce function allows us to specify duration in which the event of the same type appear in the stream we dont want to react. For example Keyinput Completion (0.5 seconds delay after key input then we do async calls). This save some performance because we dont want to react on every key typed.

Expand

Merge
Combining 2 streams of different Object types, then the result will be a combined Object. 

zipWith
This is similar merge but the difference is it creates a pair. So it will wait for the item from both streams as a pair, synchronously.

combineLatest
Always pair 2 streams latest data states

expectLater 
Subscribe to a Stream.

broadcast
Allows you to have multiple subscription to a stream.

FLAVORS
In the Production and Development environments we have API calls links which are different. You dont want to make a mistake calling which one.

The most basic way is to swap the main function:
main_dev.dart
main_prod.dart

So then you have FlavorConfig which will swap the type of Flavor :
For example:
Flavor = {DEV, PROD};

Then you can have different base URL for whichever environment/Flavor you choose.
The Singleton constructor will ensure you only run 1 environment at a time.

Then you can configure your VSCode to choose Flavor.
And it will force having a banner DEV/PROD on your UI, so you know you are not touching the PROD server.

HOW?
flutter run --flavor dev -t lib/main_dev.dart 
flutter run --flavor prod -t lib/main_prod.dart 

VSCODE:
launch.json in VSCode you can run it like this:

{
name: Flavor dev
request: launch
type: dart
program: lib/main_dev.dart
args: [
    "--flavor",
    "dev"
]
}

You can assign custom Run operation in the VSCode. 
There are no standard approach including Flutter team does not push a standard for this. So you will see variety flavor implementation. But this according to Dima is good enough.

## DAY 8



# demo_video_app

Demo video app

## Getting Started

####  1. Create flutter app
```
flutter create demo_app
```

#### 2. Create public Github repository

#### 3. Send link to your repository via email: <dpolish@aux.dkatalis.com> and <ovegn@aux.dkatalis.com>

[Send email](mailto:dpolish@aux.dkatalis.com;ovegn@aux.dkatalis.com)

## Demo app tasks

#### 1. Get api key from [omdbapi](http://www.omdbapi.com/apikey.aspx)

#### 2. Check api samples:
[search sample](http://www.omdbapi.com/)


#### 3. Import api and and env to Postman

* [demo-video-ws.postman_collection.json](/demo-video-ws.postman_collection.json)


* [demo-video-ws_Env.postman_environment.json](/demo-video-ws_Env.postman_environment.json)

#### 4. Review demo-video-ws api in Postman

#### 5. Implement api search movie by title and year 

    `?s=Robin&y=2019&apikey=<api key>`

#### 6. Implement demo-video-ws api

* `GET /recommended`

  headers:
  ```
  "Authorization": "2" - or any other string
  "Content-Type": "application/json"
  ```
  response:
  ```json
  {
  "title": "Robin Hood",
  "year": "2010",
  "id": "tt0955308",
  "poster": "https://m.media-amazon.com/images/M/MV5BMTM5NzcwMzEwOF5BMl5BanBnXkFtZTcwNjg5MTgwMw@@._V1_SX300.jpg"
  }
  ```

* `GET /videos`

    headers:
    ```
    "Authorization": "2" - or any other string
    "Content-Type": "application/json"
    ```
    response:
    ```json
    [
    {
    "label": "my film",
    "priority": 1,
    "viewed": false,
    "rating": 10, 
    "timestamp": 1575473859,
    "title": "Robin Hood",
    "year": "2010",
    "id": "tt0955308",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTM5NzcwMzEwOF5BMl5BanBnXkFtZTcwNjg5MTgwMw@@._V1_SX300.jpg"
    }
    ]
    ```

* `POST /videos`
    headers:
    ```
    "Authorization": "2" - or any other string
    "Content-Type": "application/json"
    ```
    request:
    ```json
    {
    "label": "my film",
    "priority": 1,
    "viewed": false,
    "rating": 10, 
    "timestamp": 1575473859,
    "title": "Robin Hood",
    "year": "2010",
    "id": "tt0955308",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTM5NzcwMzEwOF5BMl5BanBnXkFtZTcwNjg5MTgwMw@@._V1_SX300.jpg"
    }
    ```

* `PUT /videos/<video id>`

    headers:
    ```
    "Authorization": "2" - or any other string
    "Content-Type": "application/json"
    ```
    request:
    ```json
    {
    "label": "my film",
    "priority": 1,
    "viewed": false,
    "rating": 10, 
    "timestamp": 1575473859,
    "title": "Robin Hood",
    "year": "2010",
    "id": "tt0955308",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTM5NzcwMzEwOF5BMl5BanBnXkFtZTcwNjg5MTgwMw@@._V1_SX300.jpg"
    }
    ```

* `DELETE /videos/<video id>`

    headers:
    ```
    "Authorization": "2" - or any other string
    "Content-Type": "application/json"
    ```

* `GET /videos/<video id>`

    headers:
    ```
    "Authorization": "2" - or any other string
    "Content-Type": "application/json"
    ```
    
    response:
    ```json
    {
    "label": "my film",
    "priority": 1,
    "viewed": false,
    "rating": 10, 
    "timestamp": 1575473859,
    "title": "Robin Hood",
    "year": "2010",
    "id": "tt0955308",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTM5NzcwMzEwOF5BMl5BanBnXkFtZTcwNjg5MTgwMw@@._V1_SX300.jpg"
    }
    ```

#### 7. Splash page
- Show some info about your app, and move to next page.
- Next page depends from session token if it exist then move to home screen, in other case move to login screen
- Store session token is SharedPreference

#### 8. Login page
- Enter token, save sesion token in shared preference, and move to home screen

#### 9. Dashboard page
- Show 3 films from favorite list with highest priority with viewed=false

- And one recomendation from server
GET /recommended

#### 9. Search Page
- Filter result with search query and year see #3 task
- Shows list of films. - Move to create / edit page when user tap on a film card
- Show only 1 page of result for the task

#### 10. Favorite Movies
- Show all favorites movies sordes by timestamp
- User can remove item 
- Item onTap should redirect to Create/Edit Favorite Movie page

#### 11. Create / Edit page

    User can edit: 
    ```
    label: //user label
    priority: 0/1/2 // whete 2 - "low", 1 - "medium", 0 - "high"
    viewed: false/true
    rating: 0 - not set, 1 - 10 - your rating
    timestamp // unixtime change each time when user edit info
    ```

    other data comes from omdbapi
    ```
    "title": "Robin Hood",
    "year": "2010",
    "id": "tt0955308",
    "poster": "https://m.media-amazon.com/images/M/MV5BMTM5NzcwMzEwOF5BMl5BanBnXkFtZTcwNjg5MTgwMw@@._V1_SX300.jpg"
    ```

#### 12. Setting page
- Show recommendation on dashboard page checkbox
Save the setting in shared preference
 
- Check by default = true. If checked then user shoud see recomendation movie on dashboard.
 
- Button "Logout" it will remove token from shared preference and redirect to login page
